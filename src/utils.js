var BN = require('bignumber.js');
var util = require('util');
const BASE58 = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz'
var base58 = require('base-x')(BASE58);
var CryptoJS = require('crypto-js');
const enc = CryptoJS.enc;
const SHA256 = CryptoJS.SHA256;


const hexRegex = /^([0-9A-Fa-f]{2})*$/;
const maxTransactionAttributeSize = 65535;
const ab2str = buf => { return String.fromCharCode.apply(null, new Uint8Array(buf)); }

const str2ab = str => {
  var bufView = new Uint8Array(str.length);
	for (var i = 0, strLen = str.length; i < strLen; i++) {
		bufView[i] = str.charCodeAt(i);
	}
	return bufView;
}

const hexstring2ab = str => {
  var result = [];
	while (str.length >= 2) {
		result.push(parseInt(str.substring(0, 2), 16));
		str = str.substring(2, str.length);
	}

	return result;
}

const ab2hexstring = arr => {
  var result = "";
  for (var i = 0; i < arr.length; i++) {
    var str = arr[i].toString(16);
    str = str.length == 0 ? "00" :
      str.length == 1 ? "0" + str :
        str;
    result += str;
  }
  return result;
}

const reverseArray = arr => {
  var result = new Uint8Array(arr.length);
  for (var i = 0; i < arr.length; i++) {
    result[i] = arr[arr.length - 1 - i];
  }

  return result;
}

const numStoreInMemory = (num, length) => {
  for (var i = num.length; i < length; i++) {
    num = '0' + num;
  }
  var data = reverseArray(new Buffer(num, "HEX"));

  return ab2hexstring(data);
}

const stringToBytes = str => {
  var utf8 = unescape(encodeURIComponent(str));

  var arr = [];
  for (var i = 0; i < utf8.length; i++) {
    arr.push(utf8.charCodeAt(i));
  }

  return arr;
}

const getTransferTxData = (txData) => {
  var ba = new Buffer(txData, "hex");
  const Transaction = () => {
      this.type = 0;
      this.version = 0;
      this.attributes = "";
      this.inputs = [];
      this.outputs = [];
  };

  var tx = new Transaction();

  // Transfer Type
  if (ba[0] != 0x80) return;
  tx.type = ba[0];

  // Version
  tx.version = ba[1];

  // Attributes
  var k = 2;
  var len = ba[k];
  for (let i = 0; i < len; i++) {
      k = k + 1;
  }

  // Inputs
  k = k + 1;
  len = ba[k];
  for (let i = 0; i < len; i++) {
      tx.inputs.push({
          txid: ba.slice(k + 1, k + 33),
          index: ba.slice(k + 33, k + 35)
      });
      //console.log( "txid:", tx.inputs[i].txid );
      //console.log( "index:", tx.inputs[i].index );
      k = k + 34;
  }

  // Outputs
  k = k + 1;
  len = ba[k];
  for (let i = 0; i < len; i++) {
      tx.outputs.push({
          assetid: ba.slice(k + 1, k + 33),
          value: ba.slice(k + 33, k + 41),
          scripthash: ba.slice(k + 41, k + 61)
      });
      //console.log( "outputs.assetid:", tx.outputs[i].assetid );
      //console.log( "outputs.value:", tx.outputs[i].value );
      //console.log( "outputs.scripthash:", tx.outputs[i].scripthash );
      k = k + 60;
  }

  return tx;
}
const num2hexstring = (num, size = 1, littleEndian = false) => {
    if (typeof num !== 'number') throw new Error('num must be numeric')
    if (num < 0) throw new RangeError('num is unsigned (>= 0)')
    if (size % 1 !== 0) throw new Error('size must be a whole integer')
    if (!Number.isSafeInteger(num)) throw new RangeError(`num (${num}) must be a safe integer`)
    size = size * 2
    let hexstring = num.toString(16)
    hexstring = hexstring.length % size === 0 ? hexstring : ('0'.repeat(size) + hexstring).substring(hexstring.length)
    if (littleEndian) hexstring = reverseHex(hexstring)
    return hexstring
}

const isHex = str => {
    try {
        return hexRegex.test(str)
    } catch (err) { return false }
}

const serializeTransaction = (tx, signed = true) => {
    let out = ''
    out += num2hexstring(tx.type)
    out += num2hexstring(tx.version)
    out += serializeExclusive[tx.type](tx)
    out += num2VarInt(tx.attributes.length)
    for (const attribute of tx.attributes) {
        out += serializeTransactionAttribute(attribute)
    }
    out += num2VarInt(tx.inputs.length)
    for (const input of tx.inputs) {
        out += serializeTransactionInput(input)
    }
    out += num2VarInt(tx.outputs.length)
    for (const output of tx.outputs) {
        out += serializeTransactionOutput(output)
    }
    if (signed && tx.scripts && tx.scripts.length > 0) {
        out += num2VarInt(tx.scripts.length)
        for (const script of tx.scripts) {
            out += serializeWitness(script)
        }
    }
    return out
}

const num2VarInt = (num) => {
    if (num < 0xfd) {
        return num2hexstring(num)
    } else if (num <= 0xffff) {
        // uint16
        return 'fd' + num2hexstring(num, 2, true)
    } else if (num <= 0xffffffff) {
        // uint32
        return 'fe' + num2hexstring(num, 4, true)
    } else {
        // uint64
        return 'ff' + num2hexstring(num, 8, true)
    }
};

const ensureHex = str => {
    if (!isHex(str)) throw new Error(`Expected a hexstring but got ${str}`)
};

const reverseHex = hex => {
    ensureHex(hex)
    let out = ''
    for (let i = hex.length - 2; i >= 0; i -= 2) {
        out += hex.substr(i, 2)
    }
    return out
};

const serializeTransactionInput = (input) => {
    return reverseHex(input.prevHash) + reverseHex(num2hexstring(input.prevIndex, 2))
};

const serializeTransactionOutput = (output) => {
    const value = new Fixed8(output.value).toReverseHex()
    return reverseHex(output.assetId) + value + reverseHex(output.scriptHash)
};

const serializeTransactionAttribute = (attr) => {
    if (attr.data.length > maxTransactionAttributeSize) throw new Error()
    let out = num2hexstring(attr.usage)
    if (attr.usage === 0x81) {
        out += num2hexstring(attr.data.length / 2)
    } else if (attr.usage === 0x90 || attr.usage >= 0xf0) {
        out += num2VarInt(attr.data.length / 2)
    }
    if (attr.usage === 0x02 || attr.usage === 0x03) {
        out += attr.data.substr(2, 64)
    } else {
        out += attr.data
    }
    return out
};

const serializeWitness = (witness) => {
    const invoLength = num2VarInt(witness.invocationScript.length / 2)
    const veriLength = num2VarInt(witness.verificationScript.length / 2)
    return invoLength + witness.invocationScript + veriLength + witness.verificationScript
}


class Fixed8 extends BN {
    constructor (input, base = undefined) {
        if (typeof input === 'number') input = input.toFixed(8)
        super(input, base)
    }

    toHex () {
        const hexstring = this.mul(100000000).toString(16)
        return '0'.repeat(16 - hexstring.length) + hexstring
    }

    toReverseHex () {
        return reverseHex(this.toHex())
    }

    [util.inspect.custom] (depth, opts) {
        return this.toFixed(8)
    }

    static fromHex (hex) {
        return new Fixed8(hex, 16).div(100000000)
    }

    static fromReverseHex (hex) {
        return this.fromHex(reverseHex(hex))
    }

    /**
     * Returns a Fixed8 whose value is rounded upwards to the next whole number.
     * @return {Fixed8}
     */
    ceil () {
        return new Fixed8(super.ceil())
    }

    /**
     * Returns a Fixed8 whose value is rounded downwards to the previous whole number.
     * @return {Fixed8}
     */
    floor () {
        return new Fixed8(super.floor())
    }

    /**
     * Returns a Fixed8 rounded to the nearest dp decimal places according to rounding mode rm.
     * If dp is null, round to whole number.
     * If rm is null, round according to default rounding mode.
     * @param {number} [dp]
     * @param {number} [rm]
     * @return {Fixed8}
     */
    round (dp = null, rm = null) {
        return new Fixed8(this.round(dp, rm))
    }

    /**
     * See [[dividedBy]]
     * @param {string|number|Fixed8}
     * @param {number} [base]
     * @return {Fixed8}
     */
    div (n, base = null) {
        return this.dividedBy(n, base)
    }

    /**
     * Returns a Fixed8 whose value is the value of this Fixed8 divided by `n`
     * @param {string|number|Fixed8}
     * @param {number} [base]
     * @return {Fixed8}
     * @alias [[div]]
     */
    dividedBy (n, base = null) {
        return new Fixed8(super.dividedBy(n, base))
    }

    /**
     * See [[times]]
     * @param {string|number|Fixed8}
     * @param {number} [base]
     * @return {Fixed8}
     */
    mul (n, base = null) {
        return this.times(n, base)
    }

    /**
     * Returns a Fixed8 whose value is the value of this Fixed8 multipled by `n`
     * @param {string|number|Fixed8}
     * @param {number} [base]
     * @return {Fixed8}
     * @alias [[mul]]
     */
    times (n, base = null) {
        return new Fixed8(super.times(n, base))
    }

    /**
     * See [[plus]]
     * @param {string|number|Fixed8}
     * @param {number} [base]
     * @return {Fixed8}
     */
    add (n, base = null) {
        return this.plus(n, base)
    }

    /**
     * Returns a Fixed8 whose value is the value of this Fixed8 plus `n`
     * @param {string|number|Fixed8}
     * @param {number} [base]
     * @return {Fixed8}
     * @alias [[add]]
     */
    plus (n, base = null) {
        return new Fixed8(super.plus(n, base))
    }

    /**
     * See [[minus]]
     * @param {string|number|Fixed8}
     * @param {number} [base]
     * @return {Fixed8}
     */
    sub (n, base = null) {
        return this.minus(n, base)
    }

    /**
     * Returns a Fixed8 whose value is the value of this Fixed8 minus `n`
     * @param {string|number|Fixed8}
     * @param {number} [base]
     * @return {Fixed8}
     * @alias [[sub]]
     */
    minus (n, base = null) {
        return new Fixed8(super.minus(n, base))
    }
}

const serializeClaimExclusive = (tx) => {
    if (tx.type !== 0x02) throw new Error()
    let out = num2VarInt(tx.claims.length)
    for (const claim of tx.claims) {
        out += serializeTransactionInput(claim)
    }
    return out
};

const serializeContractExclusive = (tx) => {
    if (tx.type !== 0x80) throw new Error()
    return ''
};

const serializeInvocationExclusive = (tx) => {
    if (tx.type !== 0xd1) throw new Error()
    let out = num2VarInt(tx.script.length / 2)
    out += tx.script
    if (tx.version >= 1) out += num2fixed8(tx.gas)
    return out
};

const serializeExclusive = {
    2: serializeClaimExclusive,
    128: serializeContractExclusive,
    209: serializeInvocationExclusive
};

const hash256 = (hex) => {
    if (typeof hex !== 'string') throw new Error('reverseHex expects a string')
    if (hex.length % 2 !== 0) throw new Error(`Incorrect Length: ${hex}`)
    let hexEncoded = enc.Hex.parse(hex)
    let ProgramSha256 = SHA256(hexEncoded)
    return SHA256(ProgramSha256).toString()
}

const getScriptHash= (address) => {
    var ProgramHash = base58.decode(address);
    var ProgramHexString = CryptoJS.enc.Hex.parse(ab2hexstring(ProgramHash.slice(0, 21)));
    var ProgramSha256 = CryptoJS.SHA256(ProgramHexString);
    return reverseHex(ab2hexstring(ProgramHash.slice(1,21)));
};

const getTransactionHash = (transaction) => {
    return reverseHex(hash256(serializeTransaction(transaction, false)))
}

module.exports = {
    ab2str,
    str2ab,
    hexstring2ab,
    ab2hexstring,
    reverseArray,
    numStoreInMemory,
    stringToBytes,
    serializeTransaction,
    reverseHex,
    getScriptHash,
    getTransactionHash
}