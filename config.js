const config = {
    api: 'http://172.16.0.62:5000/api/v1/wallet',
    rpc: 'http://172.16.1.113',
    rpcPort: '10332',
    assetId: {
        FANCOIN: 'c56f33fc6ecfcd0c225c4ab356fee59390af8560be0e930faebe74a6daff7c9b',
        GAS: '602c79718b16e442de58778e148d0b1084e3b2dffd5de6b7b16cee7969282de7'
    }
};
module.exports = config;