const Wallet = require('./src/index');
const utils = require('./src/utils');
const axios = require('axios');
const config = require('./config');

const toAddress = 'ANzHohFZRrAdUu7Jt4f8nzEe1RgDVJAAjh';
const privateKey = '278021229ffe7005c66dffbcfd79994429876d64f8c11a4bc91d322ee8b8f467';
//get data from api
async function getCoinsData(address) {
    const url = `${config.api}/get_balance/${address}`;
    // const url = config.api + '/api/v1/wallet/get_balance/' + address;
    const response =  await axios.get(url);
    // console.log(typeof response.data);
    let coinsData = {
        NEO: '',
        GAS: ''
    } ;
    if(!response.data) return;
    const data = response.data;
    // const balance = data.data.balance;
    // console.log('dsfsdfsd',balance);
    // console.log('tetsts',data.balance);
    // console.log(data.balance);
    if(Number(data.data.balance.FANCOIN.totals) < 0 || parseInt(data.data.balance.GAS.totals) < 0 ) return;
    coinsData.NEO = {
        "assetid": config.assetId.FANCOIN,
        "list": data.data.balance.FANCOIN.unspent,
        "balance": parseInt(data.data.balance.FANCOIN.totals),
        "name": 'NEO'
    };
    coinsData.GAS = {
        "assetid": config.assetId.GAS,
        "list": data.data.balance.GAS.unspent,
        "balance": Number(data.data.balance.GAS.totals),
        "name": 'GAS'
    }
    // console.log(coinsData);
    return coinsData;
}

async function getClaimsData(address) {
    const url = `${config.api}/claim/${address}`;
    // const url = config.api + '/api/v1/wallet/claim/' + address;
    const response =  await axios.get(url);

    // console.log(balance);

    // console.log(response.data.claims);
    if( !!response.data.data && response.data.data.claims.length <= 0 && response.data.data.available_GAS <= 0) return;
    const claimsdata = response.data.data.claims;
    let claims = [];
    claimsdata.forEach(tx => {
        let output = {
            prevHash: tx.txid,
            prevIndex: tx.index
        }
        claims.push(output);
    });
    // console.log(claims);
    let totalClaims = response.data.data.available_GAS;
    totalClaims = totalClaims;


    // console.log(coinsData);
    return {
        claimlist: claims,
        totalClaims: totalClaims
    } ;
}

// TODO: select unspent for transaction
async function transferTx(fromAddres, privateKey, toAddress, amount, asset=1) {
    try{

        /* get data from api */
        const input = await getCoinsData(fromAddres);
        let coinsData;
        if(asset == 1) coinsData = input.NEO;
        else if(asset == 2) coinsData = input.GAS;
        else return;

        /* get info account */
        const wallet = new Wallet();
        const account =  wallet.GetAccountsFromPrivateKey(privateKey);
            // console.log(account);
        const publicKey = account[0];
            // console.log('public key', publicKey.publickeyEncoded);
        var publicKeyEncode = publicKey.publickeyEncoded;


        /* create raw data */
        const txData = wallet.TransferTransaction(coinsData, publicKeyEncode, toAddress, amount);
            // console.log('txData', txData)

        /* generate signature */
        const sign = wallet.signatureData(txData, privateKey);
            // console.log('sign', sign);

        /* add signature to transaction */
        const txRawData = wallet.AddContract(txData, sign, publicKeyEncode);
            // console.log('tx', txRawData)

        /* submit raw transaction to blockchain, using axios to post rpc */
        const instance = axios.create({
            headers: {"Content-Type": "application/json"}
        });
        console.log(txRawData);

        // const jsonRpcData = {"jsonrpc": "2.0", "method": "sendrawtransaction", "params": [txRawData], "id": 4};
        const jsonRpcData = {"raw_data": txRawData};

        const result =  await instance.post(`${config.api}/send_rawtransaction`, jsonRpcData);
        console.log(result.data);

    } catch (e){
        console.log('transfer', e);
    }
};

/*
 example data for output and claim list
    const claimlist = {
     prevHash: 'b7da2881d1966860546868fc1bd2cde1d6bf0f6923627af9577a11b812acc885',
     prevIndex: 1
    };
    const output = {
        assetId: '602c79718b16e442de58778e148d0b1084e3b2dffd5de6b7b16cee7969282de7',
        value: 3.4992,
        scriptHash: '2cd8b9cd6d5846b4aeff96f9ad254d41e009224f'
    } ;
 */


// TODO: add optional amount to claims
async function claimsGas(privateKey) {
    try{
        /* construct transction */
        var construct = {
            type: 2,
            version: 0,
            attributes: [],
            inputs: [],
            outputs:
                [],
            scripts:
                [],
            claims:
                []
        }

        /* get account info from private key */
        const wallet = new Wallet();
        const account =  wallet.GetAccountsFromPrivateKey(privateKey);
        const publicKey = account[0];
        const programHash = account[0].programHash;
        const scriptHash = utils.reverseHex(programHash);
        const address = account[0].address;
        const publicKeyEncode = publicKey.publickeyEncoded;

        /* get data from api */
        const reqData = await getClaimsData(address);
        console.log(reqData);
        // if(!reqData) return;
        const output = {
            assetId: config.assetId.GAS,
            value: reqData.totalClaims,
            scriptHash: scriptHash
        };
        const list = reqData.claimlist;
        // console.log(list);

        /*add data to construct*/
        construct.outputs.push(output);
        construct.claims.push(...list);
            // console.log(construct);

        /*add signature to construct*/
        const test = utils.serializeTransaction(construct);
        const sign = wallet.signatureData(test, privateKey);
        const txRawData = wallet.AddContract(test, sign, publicKeyEncode);
            // console.log('tx', txRawData)
        // console.log('con',construct);
        const txHash = utils.getTransactionHash(construct);
        console.log('transaction hash', txHash);
        /*submit raw Tx to blockchain, uisng axios to post request. */
        const instance = axios.create({
            headers: {"Content-Type": "application/json"}
        });

        /* rpc*/
        // const jsonRpcData = {"jsonrpc": "2.0", "method": "sendrawtransaction", "params": [txRawData], "id": 4};
        // const result =  await instance.post('http://172.16.1.119:10332', jsonRpcData);
        // console.log(txRawData);
        /*api wallet*/
        const jsonRpcData = {"raw_data": txRawData};
        const result =  await instance.post(`${config.api}/send_rawtransaction`, jsonRpcData);
        console.log(result.data);
        return txHash;

    } catch (e){
        console.log(e);
    }
}

async function getInput(address) {
    const url = `${config.api}/get_balance/${address}`;
    // const url = config.api + '/api/v1/wallet/get_balance/' + address;
    const response =  await axios.get(url);

    if(!response.data) return;
    const data = response.data;
    if(Number(data.data.balance.FANCOIN.totals) < 0 || parseInt(data.data.balance.GAS.totals) < 0 ) return;

    let unspentFC = [];
    let unspentGAS = [];
    let totalsFC = data.data.balance.FANCOIN.totals;
    let totalsGAS = data.data.balance.GAS.totals;
    data.data.balance.FANCOIN.unspent.forEach((tx) => {
        unspentFC.push({
            prevHash: tx.txid,
            prevIndex: tx.index
        });
    });

    data.data.balance.GAS.unspent.forEach((tx) => {
        unspentGAS.push({
            prevHash: tx.txid,
            prevIndex: tx.index
        });
    });
    // console.log('FANCOIN', unspentFC.length, data.data.balance.FANCOIN.unspent.length);
    // console.log('GAS', unspentGAS.length, data.data.balance.GAS.unspent.length);
    return {
        FANCOIN: {
            balance: totalsFC,
            unspent: unspentFC
        },
        GAS: {
            balance: totalsGAS,
            unspent: unspentGAS
        }
    };

}

/**
 * output: [{
 *          address: 'abc...xyz,
 *          value: 100
 *      },
 *      {
 *          ...
 *      }
 * ]
 */

function getOutput(listOutput, assetId) {
    let output = [];
    let asset, totalOutput = 0;
    if(assetId == 1) asset = config.assetId.FANCOIN;
    else asset = config.assetId.GAS
    listOutput.forEach((ot) => {
        output.push({
            assetId: asset,
            value: ot.value,
            scriptHash: utils.getScriptHash(ot.address)
        });
        totalOutput += ot.value;
    });
    // console.log('function',output.length);
    return {
        output: output,
        totalOutput: totalOutput
    }

}

async function sendMultiOutput(privateKey, output, asset = 1) {
    try{
        /* construct transction */
        var construct = {
            type: 0x80,
            version: 0,
            attributes: [],
            inputs: [],
            outputs:
                [],
            scripts:
                []
        }

        /* get account info from private key */
        const wallet = new Wallet();
        const account =  wallet.GetAccountsFromPrivateKey(privateKey);
        const publicKey = account[0];
        const programHash = account[0].programHash;
        const scriptHash = utils.reverseHex(programHash);
        const address = account[0].address;
        const publicKeyEncode = publicKey.publickeyEncoded;


        let assetId = 'FANCOIN';
        if(asset == 2) {
            assetId = 'GAS';
        }

        //ADD INPUT
        const input = await getInput(address);
        // console.log('input',input);
        construct.inputs.push(...input[assetId].unspent);
        /*add data to construct*/
        // console.log('construct',construct)
        let listOutput = getOutput(output, asset);
        if(listOutput.totalOutput > input[assetId].balance) return;
        if(listOutput.totalOutput < input[assetId].balance) {
            let change = input[assetId].balance - listOutput.totalOutput;
            // console.log('change', change, listOutput.totalOutput);
            listOutput.output.push({
                assetId: config.assetId[assetId],
                value: change,
                scriptHash: scriptHash
            });
        }
        construct.outputs.push(...listOutput.output);
        // console.log('output',listOutput.output);
        // construct.claims.push(...list);
        // console.log(construct);

        /*add signature to construct*/
        const test = utils.serializeTransaction(construct);
        const sign = wallet.signatureData(test, privateKey);
        const txRawData = wallet.AddContract(test, sign, publicKeyEncode);
        // console.log('sign',txRawData);
        // console.log('tx', txRawData)
        // console.log('con',construct);
        /*submit raw Tx to blockchain, uisng axios to post request. */

        const txHash = utils.getTransactionHash(construct);
        console.log('transaction hash', txHash);
        const instance = axios.create({
            headers: {"Content-Type": "application/json"}
        });

        /* rpc*/
        // const jsonRpcData = {"jsonrpc": "2.0", "method": "sendrawtransaction", "params": [txRawData], "id": 4};
        // const result =  await instance.post('http://172.16.1.119:10332', jsonRpcData);
        // console.log(txRawData);
        /*api wallet*/
        const jsonRpcData = {"raw_data": txRawData};
        const result =  await instance.post(`${config.api}/send_rawtransaction`, jsonRpcData);
        console.log(result.data);
        return txHash;

    } catch (e){
        console.log('error send multi user', e);
    }
}


const generatePrivateKey = () => {
    let wallet = new Wallet();
    const privateKey = wallet.generatePrivateKey();
    console.log(privateKey);
    return Buffer.from(privateKey).toString('hex');
};

const recoverWallet = (privk) => {
     let wallet = new Wallet();
     const account = wallet.GetAccountsFromPrivateKey(privk)
     return account;
}

const encryptWallet = (privk, password) => {
    let wallet = new Wallet();
    const encrypt = wallet.encryptWallet(privk, password);
    return encrypt;
};

const decryptWallet = (data, password) => {
    let wallet = new Wallet();
    const encrypt = wallet.decrypt(data, password);
    return encrypt;
}

// const privatekey = '967a398d8310ad85cbba97259b152a2669f4185bab85c128d29358ca342b86c9';
// // const priv = generatePrivateKey();
// // console.log(priv.length);
// // console.log(Buffer.from(priv, 'hex'));
// // console.log(generatePrivateKey());
//
// let test = encryptWallet(privateKey, '12345678');
// let a1 = decryptWallet(test, '12345678');
// console.log(a1);
// // let account = recoverWallet(privatekey);
// // console.log(account);
// const example = [{
//     address: 'AR81VAmzW1Dyo7PBTm5mCBnvKkQRrvBFfR',
//     value: 50
//     },
//     {
//         address: 'AJ8uDGgKSMEXPQrLdgf64D2kqfrowRQxFZ',
//         value: 50
//     }
//     ]


// sendMultiOutput(privateKey,example,1);
// claimsGas(privateKey);