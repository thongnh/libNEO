Cấu trúc folder:
   src/utils.js
   src/index.js
   index.js
   config.js
   package.json 


src/
    utils.js: chứa các function dùng để xử lý dữ liệu 
    index.js: các function dùng các hàm của utils.js để tạo transactio 
config.js: các config cần dùng để tạo transaction
index.js : các function tạo transaction


1. Các config trong file config.js:
   api: api dùng để lấy các unspent transaction output và danh sách claim gas (dùng project : neon-wallet-db để tạo)
   rpc: ip của rpc dùng để submit transaction
   rpcPort: port của rpc
   assetId: danh sách id các asset, hiện tại có NEO và GAS. 

2. Tạo transaction transfer:
   Dùng function transferTx(fromAddres, privateKey, toAddress, amount, asset=1) trong file index.js
   Danh sách các tham số:
     - fromAddress: địa chỉ người gửi
     - privateKey: privateKey của người gửi. 
     - toAddres: địa chỉ người nhận. 
     - amount: số lượng NEO hoặc GAS gửi đi 
          * Lưu ý: nếu chọn asset là GAS thì đơn vị amount của GAS được dùng là satoshi ( 1 GAS = 10^8 satoshi)
     - asset: 
        - 1 : NEO
        - 2 : GAS

3. Tạo transaction Claims Gas:
   Dùng function  claimsGas(privateKey) 
   Danh sách các tham số:
      - privateKey: privatekey của wallet. 
      * Transaction sẽ claim tất cả các gas có thể của user


-- Hết --     